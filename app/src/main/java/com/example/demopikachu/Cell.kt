package com.example.demopikachu

data class Cell(
    var row: Int,
    var col: Int,
    var value: Int,
    var centerX: Float,
    var centerY: Float,
    var width: Float,
    var height: Float
)

fun Cell.contain(x: Float, y: Float): Boolean {
    val left = centerX - width / 2f
    val right = centerX + width / 2f
    val top = centerY - height / 2f
    val bottom = centerY + height / 2f

    if (x in left..right && y in top..bottom){
        return true
    }
        return false
}