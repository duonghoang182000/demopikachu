package com.example.demopikachu

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.view.View

class GameDrawer(val view: View) {
    private var rectFSquare: RectF = RectF()
    private val paint = Paint()
    private var rectText = Rect()
    fun draw(canvas: Canvas, cells: List<Cell>, rows: Int, cols: Int, textSize: Float, colorBorder: Int, strokeWidth: Float, textColor : Int) {
        drawBorder(canvas, rows, cols, colorBorder, strokeWidth)
        drawTextNumbers(canvas, cells, textSize, textColor)
    }

    private fun drawBorder(canvas: Canvas, rows: Int, cols: Int, colorBorder : Int, strokeWidth : Float) {
        paint.color = colorBorder
        paint.strokeWidth = strokeWidth
        paint.style = Paint.Style.STROKE
        rectFSquare.left = 0F
        rectFSquare.top = 0F
        rectFSquare.right = view.width.toFloat()
        rectFSquare.bottom = view.height.toFloat()
        canvas.drawRect(rectFSquare, paint)
        val cellWidth = view.width / cols
        val cellHeight = view.height / rows
        val startRowLineX = rectFSquare.left
        var startRowLineY: Float
        val stopRowLineX = rectFSquare.right
        for (i in 1 until rows) {
            startRowLineY = rectFSquare.top + cellHeight * i
            canvas.drawLine(startRowLineX, startRowLineY, stopRowLineX, startRowLineY, paint)
        }

        var startColLineX: Float
        val startColLineY = rectFSquare.top
        val stopColLineY = rectFSquare.bottom
        for (i in 1 until cols) {
            startColLineX = rectFSquare.left + cellWidth * i
            canvas.drawLine(startColLineX, startColLineY, startColLineX, stopColLineY, paint)
        }
    }

    private fun drawTextNumbers(
        canvas: Canvas,
        cells: List<Cell>,
        textSize: Float,
        textColor : Int
    ) {
        paint.color = textColor
        paint.textSize = textSize
        cells.forEach {
            paint.getTextBounds(it.value.toString(), 0, it.value.toString().length, rectText)
            canvas.drawText(
                it.value.toString(),
                it.centerX - paint.measureText(it.value.toString()) / 2,
                it.centerY - ((paint.descent() + paint.ascent()) / 2),
                paint
            )
        }
    }


    fun drawSelected(canvas: Canvas, cells: List<Cell>) {
        paint.color = view.resources.getColor(R.color.teal_700)
        paint.style = Paint.Style.FILL
        cells.forEach {
            canvas.drawRect(
                it.centerX - it.width / 2, it.centerY - it.height / 2,
                it.centerX + it.width / 2, it.centerY + it.height / 2, paint
            )
        }

    }


}