package com.example.demopikachu

import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.example.demopikachu.listeners.OnSelectedCellListener


class GameTouchHelper(private val view: View, val listener: OnSelectedCellListener) {
    private val gestureListenerImpl = GestureListenerImpl()
    val gestureDetector = GestureDetector(gestureListenerImpl)

    fun onTouch(event: MotionEvent, cells: List<Cell>, rows: Int, cols: Int): Boolean {
        gestureListenerImpl.cells = cells
        gestureListenerImpl.rows = rows
        gestureListenerImpl.cols = cols
        return gestureDetector.onTouchEvent(event)
    }

    inner class GestureListenerImpl : GestureDetector.OnGestureListener {
        var cells: List<Cell> = emptyList()
        var rows: Int = 0
        var cols: Int = 0
        override fun onDown(e: MotionEvent?): Boolean {
            return true
        }

        override fun onShowPress(e: MotionEvent?) {

        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            if (e == null) return false
            cells.firstOrNull { it.contain(e.x, e.y) }?.let {
                listener.onCellClicked(it)
            }
            return true
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent?,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            return false
        }

        override fun onLongPress(e: MotionEvent?) {

        }

        override fun onFling(
            e1: MotionEvent?,
            e2: MotionEvent?,
            velocityX: Float,
            velocityY: Float
        ): Boolean {
            return false
        }
    }
}