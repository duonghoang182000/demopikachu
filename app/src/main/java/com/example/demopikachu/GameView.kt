package com.example.demopikachu

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.example.demopikachu.listeners.OnSelectedCellListener

class GameView : View, OnSelectedCellListener {
    private val scenceGenertor = ScenceGenertor()

    private var rows: Int = 0
    private var cols: Int = 0
    private var cells: List<Cell> = ArrayList()
    private var cellSelected = ArrayList<Cell>()
    private var application: Context
    private val touchHelper = GameTouchHelper(this, this)
    private var gameDrawer = GameDrawer(this)

    constructor(application: Context) : this(application, null)
    constructor(application: Context, attrs: AttributeSet?) : super(application, attrs) {
        this.application = application
    }

    init {
        start(6, 6)
    }

    fun start(rows: Int, cols: Int) {
        this.rows = rows
        this.cols = cols
        cells = scenceGenertor.gen(rows, cols)
        measureCells(cells)
        if (width > 0 && height > 0) {
            invalidate()
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        measureCells(cells)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        gameDrawer.drawSelected(canvas, cellSelected)
        gameDrawer.draw(
            canvas,
            cells,
            rows,
            cols,
            Utils.convertDipsToPixels(application, 16).toFloat(),
            this.resources.getColor(R.color.black),
            Utils.convertDipsToPixels(application, 2).toFloat(),
            this.resources.getColor(R.color.black)
        )

    }

    override fun onCellClicked(cell: Cell) {
        cellSelected.add(cell)
        invalidate()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return touchHelper.onTouch(event, cells, rows, cols)
    }

    private fun measureCells(cells: List<Cell>) {
        val cellWidth = this.width / cols
        val cellHeight = this.height / rows
        var xText: Int
        var yText: Int
        for (i in 0 until rows ) {
            yText = cellHeight / 2 + cellHeight * i
            xText = (cellWidth / 2)
            for (j in 0 until cols) {
                xText = cellWidth / 2 + cellWidth * j
                cells.forEach {
                    if (it.row == i+1 && it.col == j+1) {
                        it.centerX = xText.toFloat()
                        it.centerY = yText.toFloat()
                        it.width = cellWidth.toFloat()
                        it.height = cellHeight.toFloat()
                    }
                }
            }
        }
        invalidate()
    }
}