package com.example.demopikachu

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*

class GameViewModel : ViewModel() {
    private var _isWin : MutableLiveData<Boolean> = MutableLiveData()
    val isWin : LiveData<Boolean> get() = _isWin
    private var _score : MutableLiveData<Int> = MutableLiveData()
    val score : LiveData<Int> get() = _score
    val scope = CoroutineScope(Dispatchers.Default)
    var job : Job? = null

    fun playAgain(){

    }

    private fun startTimer(){
        if(job != null) return
        job = scope.launch {
            var time = 0
            while (time < 60){
                delay(1000)
                time += time + 1
            }
        }
    }
}