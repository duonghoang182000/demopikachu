package com.example.demopikachu

import android.app.Application
import android.content.Context
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class MyApplication : Application(){
    companion object {
        private var _appContext: Context? = null
        val appContext: Context? get() = _appContext
    }


    override fun onCreate() {
        super.onCreate()
        _appContext = this

    }
}
