package com.example.demopikachu

class ScenceGenertor {
    fun gen(rows: Int, cols: Int): List<Cell> {
        val cellsArray = ArrayList<Cell>()
        val genNumber = ArrayList<Int>()
        for (i in 0 until (rows * cols)/2 ) {
            genNumber.add(i + 1)
            genNumber.add(i + 1)
        }
        for (i in 1 until rows + 1) {
            for (j in 1 until cols + 1) {
                val numberRandom = genNumber.random()
                cellsArray.add(Cell(i, j, numberRandom, 0F, 0F, 0F, 0F))
                genNumber.remove(numberRandom)
            }
        }
        return cellsArray
    }
}