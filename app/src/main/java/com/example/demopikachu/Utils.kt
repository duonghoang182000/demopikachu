package com.example.demopikachu

import android.content.Context
import android.util.DisplayMetrics




class Utils {
    companion object {
        fun convertDipsToPixels(context: Context, dp: Int): Int {
            return (dp * context.resources.displayMetrics.density).toInt()
        }

        fun convertPixelsToDips(context: Context, px: Int): Int {
            return (px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
        }
    }
}