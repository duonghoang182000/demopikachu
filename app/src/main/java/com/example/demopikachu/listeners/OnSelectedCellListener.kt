package com.example.demopikachu.listeners

import com.example.demopikachu.Cell

interface OnSelectedCellListener {
    fun onCellClicked(cell:Cell)
}